import requests
import time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
import json
from datetime import datetime

# Initialisez le client InfluxDB
token = "17tX-3DncY3C3L2J9-yu8ozECVlNOI4378eIx4rIJViJVyRMpOdFW64RCsyK7jtFlVuMvwOVK9JiIJZik_XBOA=="
org = "ORN"
bucket = "ORN"

# Fonction pour pousser les données dans InfluxDB
def push_data_to_influxdb(data):
   
    point_list = []
    for capteur in data["capteurs"]:

        if not capteur["active"]:
            continue


        for index, definition in enumerate(capteur["def"] if isinstance(capteur["def"], list) else [capteur["def"]]):
                
            point = Point(data["groupe"]) \
                .tag("name", data["name"]) \
                .tag("sensor_name", capteur["name"]) \
                .tag("definition", definition)

            if "values" in capteur:
                if not isinstance(capteur["values"], list):
                    capteur["values"] = [capteur["values"]]
                point = point.field("value", capteur["values"][index])
                print(index, definition, capteur["values"][index])
            elif "error" in capteur:
                point = point.field("error", capteur["error"])
            else:
                print("WARNING no value or error to return....")
                point = point.field("error", "no value or error to return")
                
            #point.time(timestamp)
            point_list.append(point)
    return point_list


def send_http_request(ip_address):
    url = f"http://{ip_address}"
    try:
        response = requests.get(url)
        if response.status_code == 200:
            print("Requête envoyée avec succès.")
            return response.json()  # Renvoie la réponse JSON
        else:
            print(f"Erreur lors de l'envoi de la requête : {response.status_code}")
            return None
    except requests.exceptions.RequestException as e:
        print(f"Une erreur s'est produite : {e}")
        return None


def main():
    microcontroller_ip = '192.168.1.42'  # Mettre ici l'adresse IP de notre microcontrôleur
    while True:
        client = InfluxDBClient(url="http://localhost:8086", token=token)
        timestamp = int(datetime.utcnow().timestamp())
        write_api = client.write_api(write_options=SYNCHRONOUS)
        response_json = send_http_request(microcontroller_ip)
        if response_json:
            print("Réponse reçue du microcontrôleur :")
            print(response_json)
            points = push_data_to_influxdb(response_json)
            print("Data pushed successfully!")
            print("points", points)
            for index, point in enumerate(points):
                print(index, point)
                write_api.write(bucket=bucket, org=org, record=point)
            client.close()
        else:
            print("Pas de réponse reçue du microcontrôleur.....")
        time.sleep(2)  # Envoyer une requête toutes les 5 secondes


main()
